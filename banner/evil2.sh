#!/bin/bash

Red='\033[0;31m'   #Red
g='\033[0;32m'     #Green
p='\033[0;35m'     #Purple
BBlue='\033[1;34m' #Bold Blue
none='\033[0m'     #none/no color
BG='\033[1;32m'    #Bold Green
BR='\033[1;31m'    #Bold Red

printf "$BG                                                                       $none\n"
printf "$BG _____       _ _      _                _ _           _   _             $none\n"
printf "$BG| ____|_   _(_) |    / \   _ __  _ __ | (_) ___ __ _| |_(_) ___  _ __  $none\n"
printf "$BG|  _| \ \ / / | |   / _ \ | '_ \| '_ \| | |/ __/ _' | __| |/ _ \| '_ \ $none\n"
printf "$BG| |___ \ V /| | |  / ___ \| |_) | |_) | | | (_| (_| | |_| | (_) | | | |$none\n"
printf "$BG|_____| \_/ |_|_| /_/   \_\ .__/| .__/|_|_|\___\__,_|\__|_|\___/|_| |_|$none\n"
printf "$BG                          |_|   |_|                                    $none\n"
printf "$BG                                                                       $none\n"
printf "$BR                                                      By: Johnsmith    $none\n"
